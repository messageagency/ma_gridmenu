# MA D8 Grid Menu #

This module and core patch patch provides the necessary codebase to add grid-style children menus to Drupal 8 sites.

Version 1.1

## Installation ##
* Download repo as zip and install as you would any other module (In D8, this means copying the module into the modules or modules/custom directory).
* Apply the core patch. The patch addresses the concerns detailed here: https://www.drupal.org/node/2631468. The patch allows us to follow the current page's entry in the main menu and use it as the bass from which children links are used in the menu. Hopefully this patch will be RTBC soon and merged into core.
* Create the Main Menu Block through the interface, noting the machine name for the block. Make sure to check off the "Make the starting level follow the active menu item." checkbox in the block config, and only use one level for depth.
* Navigate to Structure > Grid Menu Settings (/admin/structure/gridmenu/settings)
* Select one or more of the listed blocks to apply the grid menu layout to. Only menu blocks will be listed (both system menu blocks and menu blocks created using the Menu Block module).
* Save the settings.

## Notes for Themers ##
* The base HTML for the content of the block is is located in the grid-menu.html.twig file in the templates directory.
* The HTML has ben re-written by Andre reflecting lessons learned theming the Gearing Up site.
* All of the class names can be found here for attaching CSS to.
* The image for the menu item is inserted into the anchor element as a background-image attribute. An image is only specified for applicable links: ones that belong to a piece of content with field_image field. External links and views do not have this information, for example. The background-image URL will be "" in these cases. (TODO: resolve this better  in the code)
* A (very) base CSS file has been added for the grid menu. This includes support for background colors for when there is no image. Still need to add support for a default bg image.
