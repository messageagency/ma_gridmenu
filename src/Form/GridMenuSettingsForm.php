<?php

/**
 * @file
 * Contains \Drupal\ma_gridmenu\Form\GridMenuSettingsForm
 */

namespace Drupal\ma_gridmenu\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class GridMenuSettingsForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ma_gridmenu_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'ma_gridmenu.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ma_gridmenu.settings');
    $options = array();
    $ids = \Drupal::entityQuery('block')->execute();

    foreach ($ids as $id) {
      $block = \Drupal::entityTypeManager()
        ->getStorage('block')
        ->load($id);
      if (strpos($block->getPluginId(), 'system_menu_block:') !== FALSE ||
        strpos($block->getPluginId(), 'menu_block:') !== FALSE) {
        $options[$id] = $id . ' (label: ' . $block->label() . ')';
      }
    }

    $form['menu_machine_name'] = array(
      '#type' => 'select',
      '#title' => $this->t('Menu\'s Machine Name'),
      '#options' => $options,
      '#default_value' => $config->get('menu.machine-name'),
      '#multiple' => TRUE,
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = \Drupal::service('config.factory')->getEditable('ma_gridmenu.settings');
    $config->set('menu.machine-name', $form_state->getValue('menu_machine_name'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}
