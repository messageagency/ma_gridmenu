<?php

namespace Drupal\ma_gridmenu;

use \Drupal\Core\Render\Element\RenderCallbackInterface;
use \Drupal\node\Entity\Node;

/**
 * Provides a trusted callback to alter the commerce cart block.
 *
 * @see ma_gridmenu_block_view_system_menu_block_alter()
 * @see ma_gridmenu_block_view_menu_block_alter()
 */
class GridMenuBlockViewBuilder implements RenderCallbackInterface {

  /**
   * Sets Grid Menu common - #pre_render callback.
   */
  public static function preRender(array $elements) {
    if (empty($elements['content']['#items'])) {
      return $elements;
    }
    $items = &$elements['content']['#items'];
    foreach($items as $item_id => $item) {
      $url = $item['url'];
      try {
        $params = $url->getRouteParameters();
      } catch (\Exception $e) {
        $params = array();
      }

      $nid = (array_key_exists('node', $params)) ? $params['node'] : '0';
      if ($nid != '0') {
        $entity =  Node::load($nid);
        $img = $entity->field_image->entity;
        $ipath = (!is_null($img)) ? $img->createFileUrl() : '';
        if(!is_string($ipath)) { $ipath = ''; }

      } else {
        $ipath = '';
      }
      // $item['attributes']->setAttribute('style','background-image: url(' . $ipath . ');');
      $item['attributes']->setAttribute('data-bg-img', $ipath);
      $item['attributes']->setAttribute('data-bg-img-style', NULL);
    }
    $elements['content']['#theme'] = 'grid_menu';
    return $elements;
  }

}
